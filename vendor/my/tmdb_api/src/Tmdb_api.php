<?php

namespace My\Tmdb_api;

class Tmdb_api
{
  private $settings = null;

  public function __construct($settings = array())
  {
    $this->settings = $settings;
  }

  public function getPageSize()
  {
    return $this->settings['pageSize'];
  }

  public function getTopSize()
  {
    return $this->settings['topSize'];
  }

  // kulso api hivas inditasa

  private function execute($method = "GET", $endpoint = null, $params = array())
	{
		$result = array();
    $built_params = array('api_key' => $this->settings['apiKey']);
    $built_params = array_merge($built_params, $params);

		if (isset($endpoint))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_SSLVERSION, 4);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      switch($method)
      {
        case 'POST':
                    curl_setopt($ch, CURLOPT_URL, $this->settings['url'].$endpoint);
                    curl_setopt($ch, CURLOPT_POST, 1);
  			            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($built_params));
                    break;
        default: //GET
                    curl_setopt($ch, CURLOPT_URL, $this->settings['url'].$endpoint.'?'.http_build_query($built_params));
                    curl_setopt($ch, CURLOPT_POST, 0);
                    break;
      }
			$request = curl_exec($ch);
      $parse = json_decode($request,true);
      switch (json_last_error())
      {
         case JSON_ERROR_NONE:
             $error = '';
         break;
         case JSON_ERROR_DEPTH:
             $error = ' - Maximum stack depth exceeded';
         break;
         case JSON_ERROR_STATE_MISMATCH:
             $error = ' - Underflow or the modes mismatch';
         break;
         case JSON_ERROR_CTRL_CHAR:
             $error = ' - Unexpected control character found';
         break;
         case JSON_ERROR_SYNTAX:
             $error = ' - Syntax error, malformed JSON '.$request;
         break;
         case JSON_ERROR_UTF8:
             $error = ' - Malformed UTF-8 characters, possibly incorrectly encoded';
         break;
         default:
             $error = ' - Unknown error';
         break;
      }
      if (!empty($error))
      {
        throw new exception($error);
      }
      else
      {
				$result = $parse;
      }
			curl_close($ch);
		}
		return $result;
	}


  // toplista adott oldalanak lekerdezese

  public function getTopRatedMoviesPage($page = 1, $language = 'hu-HU', $region = null)
  {
    $result = $params = array();
    $page = (int)$page;
    $params['page'] = max(1,min($page,1000));
    if(!empty($language))
    {
      $params['language'] = $language;
    }
    if(!empty($region))
    {
      $params['region'] = $region;
    }
    try
    {
      $json = $this->execute('GET','movie/top_rated',$params);
      if (!empty($json['results']))
      {
        $result = $json['results'];
      }
    }
    catch (\Exception $e)
    {
      log_message('error', "Nem sikerült letölteni a topRatedMovies oldalt [".$page."]. (".$e->getMessage().")");
    }
    return $result;
  }

  // egyben lekerdezzuk az elso 210 filmet (20 db van egy oldalon)
  public function getTopRatedMovies()
  {
    $topMovies = array();
    $count = $this->getTopSize();
    $pageSize = $this->getPageSize();
    $pageCount = ceil($count / $pageSize);
    try
    {
      for($i = 1; $i <= $pageCount; $i++)
      {
        $pageMovies = $this->getTopRatedMoviesPage($i);
        // ures oldal jon vissza, akkor nem lesz meg a sorrend => hiba
        if(empty($pageMovies))
        {
          throw new exception('Toplista frissítés sikertelen. Soron következő blokkra nem jött válasz.');
        }
        $topMovies = array_merge($topMovies, $pageMovies);
      }
      // 210 felettieket eldobjuk
      $topMovies = array_slice($topMovies,0, $count);
    }
    catch (\Exception $e)
    {
      log_message('error', "Hiba a toplista lekérdezésekor.  (".$e->getMessage().")");
    }
    return $topMovies;
  }

  public function getGenres($language = 'hu-HU')
  {
    $genres = array();
    if(!empty($language))
    {
      $params['language'] = $language;
    }
    try
    {
      $json = $this->execute('GET','genre/movie/list',$params);
      if (!empty($json['genres']))
      {
        $genres = $json['genres'];
      }
    }
    catch (\Exception $e)
    {
      log_message('error', "Nem sikerült letölteni a film tipusok listáját. (".$e->getMessage().")");
    }
    return $genres;
  }

  public function getConfiguration()
  {
    $json = array();
    try
    {
      $json = $this->execute('GET','configuration',array());
    }
    catch (\Exception $e)
    {
      log_message('error', "Nem sikerült letölteni a film tipusok listáját. (".$e->getMessage().")");
    }
    return $json;
  }


  // film adatlap lekerese
  public function getMovieDetail($id, $language = 'hu-HU')
  {
    $result = array();
    // lekerjuk a stablistat is a rendezokhoz
    $params = array('append_to_response' => 'credits');
    if(!empty($language))
    {
      $params['language'] = $language;
    }
    try
    {
      $json = $this->execute('GET','movie/'.$id,$params);
      if(!empty($json['id']))
      {
        $result = $json;
        // ha az elsodleges nyelven nincsen leiras, akkor lekerjuk az angolt is,
        // de csak akkor hasznaljuk, ha ebben van leiras
        if(empty($json['overview']))
        {
          $params['language'] = 'en-US';
          $json = $this->execute('GET','movie/'.$id,$params);
          if(!empty($json['overview']))
          {
            $result['overview'] = $json['overview'];
          }
        }
      }
    }
    catch (\Exception $e)
    {
      log_message('error', "Sikertelen film adatlap lekérdezés: ".$e->getMessage().".");
    }
    return $result;
  }

  // $movies listaban levo osszes adatlapot lekerjuk egyesevel (nem lehet tobbet egyszerre)
  public function getMoviesDetail($movies = array())
  {
    $details = array();
    if(!empty($movies))
    {
      foreach($movies as $id)
      {
        $detail = $this->getMovieDetail($id);
        if(!empty($detail['id']))
        {
          $details[$id] = $detail;
        }
      }
    }
    return $details;
  }


  // szemely adatainak lekerese
  public function getPersonDetail($id, $language = 'hu-HU')
  {
    $result = array();
    $params = array();
    if(!empty($language))
    {
      $params['language'] = $language;
    }
    try
    {
      $json = $this->execute('GET','person/'.$id,$params);
      if(!empty($json['id']))
      {
        $result = $json;
        // ha az elsodleges nyelven nincsen leiras, akkor lekerjuk az angolt is,
        // de csak akkor hasznaljuk, ha ebben van leiras
        if(empty($json['biography']))
        {
          $params['language'] = 'en-US';
          $json = $this->execute('GET','person/'.$id,$params);
          if(!empty($json['biography']))
          {
            $result = $json;
          }
        }
      }
    }
    catch (\Exception $e)
    {
      log_message('error', "Sikertelen személy adatlap lekérdezés: ".$e->getMessage().".");
    }
    return $result;
  }

  // $persons listaban levo szemelyek adatait lekerjuk egyesevel
  public function getPeopleDetail($persons = array())
  {
    $details = array();
    if(!empty($persons))
    {
      foreach($persons as $id)
      {
        $detail = $this->getPersonDetail($id);
        if(!empty($detail['id']))
        {
          $details[$id] = $detail;
        }
      }
    }
    return $details;
  }



}

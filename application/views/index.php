<main class="container mt-2">
  <div class="starter-template text-center py-5">
    <h1>Top Rated Movies</h1>

    <div class="mt-5 px-5">
      <div class="row">
        <?php if(!empty($movies)): ?>
        <?php foreach($movies as $movie): ?>
        <div class="col-xl-3 d-flex">
          <div class="movie-card mb-4 position-relative">
            <a class="" href="/movie/<?php echo $movie['id_tmdb']; ?>">
              <img class="poster" src="<?php echo str_replace('/original/','/w500/',$movie['poster_url']); ?>" />
            </a>
            <div class="info p-2">
              <a class="title " href="/movie/<?php echo $movie['id_tmdb']; ?>"><?php echo $movie['position'].'. '.$movie['title']; ?></a>
              <?php $genres = explode(', ', $movie['genres']); ?>
              <?php if(!empty($genres)): ?>
                <ul class="genres">
                <?php foreach($genres as $genre): ?>
                  <li><?php echo $genre;?></li>
                <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </div>
            <div class="stats text-start position-absolute bottom-0 start-0">
              <i class="far fa-clock"></i> <?php echo $movie['runtime']; ?> perc <span class="rating">TMDB <strong><?php echo $movie['vote_average']; ?></strong>/10</span>
              <a class="float-end" href="/movie/<?php echo $movie['id_tmdb']; ?>"><i class="far fa-arrow-alt-circle-right"></i></a>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>

    <div class="paging text-center">
      <?php for($i=1; $i <= $pages; $i++): ?>
      <a href="/page/<?php echo $i; ?>" class="btn <?php echo ($page == $i ? 'btn-secondary active' : 'btn-warning'); ?>"><?php echo $i; ?></a>
      <?php endfor; ?>
    </div>

  </div>

</main><!-- /.container -->

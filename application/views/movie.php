<main >
  <div class="row py-5 backdrop">
		<div class="col-xl-3 offset-xl-1  d-flex">
			<div class="movie-card my-5">
				<img class="poster" src="<?php echo str_replace('/original/','/w500/',$movie['poster_url']); ?>" />
			</div>
		</div>
		<div class="col-xl-8 d-flex text-light">
			<div>
				<?php if(!empty($movie['release_date'])): ?>
				<span class="mt-5 d-block"><?php echo date('Y', strtotime($movie['release_date'])); ?></span>
				<?php endif; ?>
	    	<h1 class="mt-2 fs-2"><?php echo $movie['position'].'. '.$movie['title']; ?></h1>
				<?php if(!empty($movie['genres'])): ?>
					<ul class="genres big">
					<?php foreach($movie['genres'] as $genre): ?>
						<li><?php echo $genre;?></li>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				<p class="my-5 pe-5"><?php echo $movie['overview']; ?></p>
				<div class="stats big">
					<i class="far fa-clock"></i> <?php echo $movie['runtime']; ?> perc <span class="rating big">TMDB <strong><?php echo $movie['vote_average']; ?></strong>/10</span> (<?php echo $movie['vote_count']; ?> szavazat)
					<a class="ms-5 btn btn-warning rounded-pill" target="_blank" href="<?php echo $movie['tmdb_url']; ?>">TMDB link</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-11 offset-xl-1">
			<h2 class="fs-4 mt-2">Rendező<?php echo (count($movie['directors'])> 1 ? 'k' : ''); ?></h2>
			<?php foreach($movie['directors'] as $director): ?>
				<h3 class="fs-5 mt-4"><?php echo $director['name'].(empty($director['date_of_birth']) ? '' :  (' ('.$director['date_of_birth'].')')); ?></h3>
				<p class="pe-5 mb-5"><?php echo $director['biography']; ?></p>
			<?php endforeach; ?>
		</div>
	</div>

    <div class="paging text-center mb-5">
			<a class="btn btn-secondary" href="javascript:history.go(-1);">Vissza</a>
    </div>


</main><!-- /.container -->

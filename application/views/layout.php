<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title>Top Rated Movies</title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/all.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
  <style>

    .backdrop {
      background: linear-gradient(to bottom right, rgba(6.67%, 4.71%, 4.71%, 1.00), rgba(6.67%, 4.71%, 4.71%, 0.84))<?php echo empty($movie['backdrop_url']) ? '' : ', url("'.$movie['backdrop_url'].'")';?>;
      background-size: cover;
      background-repeat: no-repeat;
    }

    .movie-card {
      background-color: #fff;
      overflow: hidden;
      position: relative;
      display: block;
    }
    .movie-card .poster {
      max-width: 100%;
      height: auto;
      display: block;
    }

    .movie-card .info
    {
      text-align: left;
      padding-bottom: 30px !important;
    }

    .movie-card .info .title
    {
      font-weight: bold;
      font-size: 14px;
      text-decoration: none;
      color: #555;
      display: block;
      width: 100%;
    }

    .genres
    {
      padding: 0;
      margin: 0;
      margin-top: -5px;
      line-height: 18px;
    }
    .genres > li
    {
      font-size: 11px;
      text-decoration: none;
      color: #888;
      display: inline-block;
      margin-right: 1.5em;
      padding: 0;
      line-height: 12px;
    }

    .genres > li:not(:first-child)::before {content: "•";
      font-size: 14px;
      display: inline-block; width: 1em;
      margin-left: -1em
    }

    .genres.big li {
      font-size: 15px;
    }

    .genres.big > li:not(:first-child)::before {
    font-size: 18px;
    }

    .stats
    {
      line-height: 20px;
      font-size: 12px;
      padding: .5rem;
      margin: 0;
      display: block;
      width: 100%;
    }

    .stats a>i
    {
      font-size: 20px;
      color: #000;
    }

    .stats.big
    {
      font-size: 15px;
    }


    .rating
    {
      margin-left: 2em;
    }

    .rating::before {content: "•";
      color: #ffcc00;
      position: relative;
      top: 5px;
      font-size: 30px;
      line-height: 15px;
      font-weight: bold;
      display: inline-block; width: 0.5em;
      margin-left: -0.5em;
    }

    .rating.big::before {
      font-size: 35px;
    }

    body {background-color: #f1f1f1;}
  </style>
  </head>
  <body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Top Rated Movies</a>
    <a class="btn btn-light float-end" href="/test">Teszt funkciók</a>
  </div>
</nav>
<?php if(!empty($Template)):?>
<?php $this->load->view($Template, $this->data); ?>
<?php endif; ?>

  <script src="/assets/js/bootstrap.min.js"></script>
  </body>
</html>

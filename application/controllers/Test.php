<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
		$this->load->model('Tmdb_model');
  }

  public function index()
  {
    $data = array();
    $data['lastUpdate'] = $this->Tmdb_model->getLastUpdate();
    $data['top10'] = $this->Tmdb_model->getTop10();
    $this->load->view('test', $data);
  }

  public function movie($id)
  {
		$this->data = array();
		$this->data['Template'] = 'movie';
    $this->data['movie'] = $this->Tmdb_model->getMovie($id);
    $this->load->view('layout', $this->data);
  }

  public function setYesterday()
  {
    $this->db->query('UPDATE cron set last_run=date(now() - interval 1 day)');
    redirect('/test');
  }

  public function resetFields()
  {
    $this->db->query('UPDATE genres SET genre=null');
    $this->db->query('UPDATE movies SET title=null,overview=null,release_date=null,tmdb_url=null,poster_url=null,
                      vote_count=null,vote_average=null,runtime=null,backdrop_url=null');
    $this->db->query('UPDATE directors SET name=null,biography=null,date_of_birth=null');
    redirect('/test');
  }

  public function truncateTables()
  {
    $this->db->query('UPDATE cron set last_run=null');
    $this->db->query('TRUNCATE genres');
    $this->db->query('TRUNCATE directors');
    $this->db->query('TRUNCATE movies');
    $this->db->query('TRUNCATE movie_genres');
    $this->db->query('TRUNCATE movie_directors');
    redirect('/test');
  }

}

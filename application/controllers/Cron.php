<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Tmdb_api\Tmdb_api;

class Cron extends CI_Controller
{

	public function __construct()
  {
    parent::__construct();
    $this->settings = $this->config->item('TMDB');
    $this->tmdb_api = new Tmdb_api($this->settings);
    $this->load->model('Tmdb_model');
  }

  public function index()
  {
    $lastUpdate = $this->Tmdb_model->requestTopRatedMovies();
    if(!empty($lastUpdate))
    {
      $config = $this->tmdb_api->getConfiguration();
      $image_url = empty($config['images']['secure_base_url']) ? null : $config['images']['secure_base_url'];
      $site_url = 'https://themoviedb.org/';
      $genres = $this->tmdb_api->getGenres();
      $this->Tmdb_model->updateGenres($genres);
      echo count($genres).' tipus frissítve.<br>';
      $movies = $this->tmdb_api->getTopRatedMovies();
      $this->Tmdb_model->updateMovies($movies, $site_url, $image_url);
      echo count($movies).' film letöltve.<br>';
    }
    $getMovieDetails = $this->Tmdb_model->getMoviesForUpdate();
    if(!empty($getMovieDetails))
    {
      $details = $this->tmdb_api->getMoviesDetail($getMovieDetails);
      $this->Tmdb_model->updateMoviesDetail($details);
      echo count($details).' film adatlap frissítve.<br>';
    }
    $getDirectorsDetails = $this->Tmdb_model->getDirectorsForUpdate();
    if(!empty($getDirectorsDetails))
    {
      $details = $this->tmdb_api->getPeopleDetail($getDirectorsDetails);
      $this->Tmdb_model->updateDirectorsDetail($details);
      echo count($details).' rendezői adatlap frissítve.<br>';
    }
    echo 'Az ismételt futtatáshoz kattints <a href="/cron">ide</a>, vagy frissítsd az oldalt!';
  }

}

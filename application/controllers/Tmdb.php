<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tmdb extends CI_Controller
{

	public function __construct()
  {
		parent::__construct();
		$this->load->model('Tmdb_model');
  }

	public function index($page = 1)
	{
		$this->data = array();
		$this->data['Template'] = 'index';
		$this->data['pages'] = ceil($this->Tmdb_model->getMovieCount() / 20);
		$this->data['page'] = $page;
		$this->data['movies'] = $this->Tmdb_model->getMovies($page, 20);
		$this->load->view('layout',$this->data);
	}

	public function movie($id)
	{
		$this->data = array();
		$this->data['Template'] = 'movie';
		$this->data['movie'] = $this->Tmdb_model->getMovie($id);
    $this->load->view('layout', $this->data);
	}

}

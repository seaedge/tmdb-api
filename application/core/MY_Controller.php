<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Controller extends CI_Controller
{

  var $data = array();
  private $_userID = 0;
  private $_langID = 1;

	public function __construct()
	{
		parent::__construct();
    header('Content-Type: text/html; charset=utf-8');
    $this->_userID = $this->session->userdata('user_id');
    if (empty($this->_userID))
    {
      if ($this->input->is_ajax_request())
      {
        if (!(strtolower($this->uri->segments[1]) == 'scale' AND strtolower($this->uri->segments[2]) == 'printpdf'))
        {
          echo json_encode(array('status' => 'error', 'message' => 'Permission denied'));
          exit;
        }
      }
      else
      {
        if (!(strtolower($this->uri->segments[1]) == 'scale' AND strtolower($this->uri->segments[2]) == 'printpdf'))
        {
          redirect('/');
        }
      }
		}

  	$this->load->model('User_model');
    $this->data['userdata']=$this->User_model->user($this->_userID);
    //$this->data['PERMISSION_ERROR'] = $this->session->userdata('PERMISSION_ERROR');
    $this->session->set_userdata('PERMISSION_ERROR','');
    $this->data['last_sync'] = $this->User_model->getLastSync();
    $this->data['jsList'] = array();
    $this->data['cssList'] = array();
    $this->data['customJS'] = '';
	}


  public function check_permission($permission = null, $redirect = false, $setAlert = true)
  {
    $allow = false;
    if (!empty($permission) AND !empty($this->data['userdata']['permissions']))
    {
      $groups = array_keys($this->data['userdata']['permissions']);
      if (is_array($permission))
      {
        if (isset($permission[0]) AND isset($permission[1]) AND
            !empty($this->data['userdata']['permissions'][$permission[0]]) AND
            is_array($this->data['userdata']['permissions'][$permission[0]]) AND
            in_array($permission[1], $this->data['userdata']['permissions'][$permission[0]]))
        {
          $allow = true;
        }
        elseif(isset($permission[0]) AND count($permission) == 1 AND in_array($permission[0],$groups))
        {
          $allow = true;
        }
      }
      else
      {
        if (in_array($permission, $groups))
        {
          $allow = true;
        }
      }
    }
    if (!$allow AND $setAlert)
    {
      $this->session->set_userdata('PERMISSION_ERROR',"You don't have ".(is_array($permission) ?  implode('.', $permission) : $permission).' permission.');
    }
    if (!$allow AND $redirect)
    {
      redirect('/dashboard');
    }
    return $allow;
  }

  function userID()
  {
    return $this->_userID;
  }

}

<?php
class Tmdb_model extends CI_Model
{
  // ekkora sql-be szervezzuk a blokkosithato parancsokat
  private $batchSize = 512 * 1024;

	public function __construct()
	{
		parent::__construct();
	}


  // toplista filmjeit betoltjuk vagy frissitjuk a tablaban, elotte lekerjuk a tipusokat
  public function updateMovies($movies, $site_url = null, $image_url = null)
  {
    if(!empty($movies) AND is_array($movies))
    {
      $this->db->query('BEGIN');
      $this->db->query('TRUNCATE movie_genres');
      $insertGenresStart = 'INSERT INTO movie_genres (id_tmdb,id_genre) VALUES ';
      $valuesGenres = '';
      $joinGenres = '';
      $insertMoviesStart = 'INSERT INTO movies (position,id_tmdb,title,overview,release_date,backdrop_url,poster_url,tmdb_url,vote_average,
                      vote_count,need_details) VALUES ';
      $insertMoviesEnd = ' ON DUPLICATE KEY UPDATE position=VALUES(position),title=VALUES(title),overview=VALUES(overview),
                    release_date=VALUES(release_date),poster_url=VALUES(poster_url),tmdb_url=VALUES(tmdb_url),
                    vote_average=VALUES(vote_average),vote_count=VALUES(vote_count),need_details=VALUES(need_details),
                    backdrop_url=VALUES(backdrop_url)';
      $valuesMovies = '';
      $joinMovies = '';
      foreach($movies as $position => $movie)
      {
        $valuesMovies.=$joinMovies.'('.$this->db->escape($position+1).','.$this->db->escape($movie['id']).',
                         '.$this->db->escape($movie['title']).','.$this->db->escape($movie['overview']).',
                         '.$this->db->escape($movie['release_date']).',
                         '.$this->db->escape($image_url.'original'.$movie['backdrop_path']).',
                         '.$this->db->escape($image_url.'original'.$movie['poster_path']).',
                         '.$this->db->escape($site_url.'movie/'.$movie['id']).','.$this->db->escape($movie['vote_average']).',
                         '.$this->db->escape($movie['vote_count']).',1)';
        $joinMovies = ', ';
        if(!empty($movie['genre_ids']))
        {
          foreach($movie['genre_ids'] as $genre_id)
          {
            $valuesGenres .= $joinGenres.'('.$this->db->escape($movie['id']).','.$this->db->escape($genre_id).')';
            $joinGenres = ', ';
          }
        }
        // ha elertuk a maximalis query meretet, akkor futtatjuk
        if(strlen($valuesMovies) > $this->batchSize)
        {
          $this->db->query($insertMoviesStart.$valuesMovies.$insertMoviesEnd);
          $joinMovies = '';
          $valuesMovies = '';
          if(!empty($valuesGenres))
          {
            $this->db->query($insertGenresStart.$valuesGenres);
            $joinGenres = '';
            $valuesGenres = '';
          }
        }
      }

      // maximalis sql meret alatti parancsok futtatasa
      if(!empty($valuesMovies))
      {
        $this->db->query($insertMoviesStart.$valuesMovies.$insertMoviesEnd);
        if(!empty($valuesGenres))
        {
          $this->db->query($insertGenresStart.$valuesGenres);
        }
      }
      // toroljuk a 210-bol kiesett filmeket
      $this->db->query('DELETE FROM movies WHERE need_details=0');
      // ma mar ne frissitse tobbszor a listat a cronjob
      $this->requestTopRatedMoviesDone();
      $this->db->query('COMMIT');
    }
  }

  // filmtipusok frissitese
  public function updateGenres($genres)
  {
    $status = false;
    if(!empty($genres) AND is_array($genres))
    {
      $this->db->query("BEGIN");
      $insertStart = 'INSERT INTO genres (id_genre,genre) VALUES ';
      $insertEnd = ' ON DUPLICATE KEY UPDATE genre=VALUES(genre)';
      $values = '';
      $join = '';
      foreach($genres as $genre)
      {
        $values.=$join.'('.$this->db->escape($genre['id']).','.$this->db->escape($genre['name']).')';
        $join = ', ';
        if(strlen($values) > $this->batchSize)
        {
          $this->db->query($insertStart.$values.$insertEnd);
          $join = '';
          $values = '';
        }
      }
      if(!empty($values))
      {
        $this->db->query($insertStart.$values.$insertEnd);
      }
      $this->db->query("COMMIT");
      $status = true;
    }
    return $status;
  }

  // toplistaban lekerdezesben nem szereplo mezok kitoltese / frissitese
  public function updateMoviesDetail($details = array())
  {
    if(!empty($details) AND is_array($details))
    {
      $this->db->query('BEGIN');
      $keyList = array();
      $insertDirectorsStart = 'INSERT INTO movie_directors (id_tmdb,id_director) VALUES ';
      $valuesDirectors = '';
      $joinDirectors = '';
      $insertMoviesStart = 'INSERT INTO movies (id_tmdb,runtime,overview,need_details) VALUES ';
      $insertMoviesEnd = ' ON DUPLICATE KEY UPDATE runtime=VALUES(runtime),overview=VALUES(overview),need_details=VALUES(need_details)';
      $valuesMovies = '';
      $joinMovies = '';
      foreach($details as $id_tmdb => $detail)
      {
        $keyList []= $id_tmdb;
        $valuesMovies.=$joinMovies.'('.$this->db->escape($id_tmdb).','
                                    .$this->db->escape(empty($detail['runtime']) ? null : $detail['runtime']).','
                                    .$this->db->escape($detail['overview']).',0)';
        $joinMovies = ', ';
        if(!empty($detail['credits']['crew']) AND is_array($detail['credits']['crew']))
        {
          foreach($detail['credits']['crew'] as $crew)
          {
            if(!empty($crew['job']) AND $crew['job'] == 'Director')
            {
              $valuesDirectors .= $joinDirectors.'('.$this->db->escape($id_tmdb).','.$this->db->escape($crew['id']).')';
              $joinDirectors = ', ';
            }
          }
        }
        if(strlen($valuesMovies) > $this->batchSize)
        {
          $this->db->query($insertMoviesStart.$valuesMovies.$insertMoviesEnd);
          $joinMovies = '';
          $valuesMovies = '';
          // rendezok adatlap frissiteset beallitjuk
          $this->db->query('UPDATE movie_directors md INNER JOIN directors d ON md.id_director=d.id_director
                            SET d.need_details=1
                            WHERE md.id_tmdb in ('.implode(',', array_map(array($this->db,'escape'), $keyList)).')');
          //toroljuk a kiesett rendezoket a filmtol
          $this->db->query('DELETE FROM movie_directors WHERE id_tmdb in ('.implode(',', array_map(array($this->db,'escape'), $keyList)).')');
          $keyList = array();
          if(!empty($valuesDirectors))
          {
            $this->db->query($insertDirectorsStart.$valuesDirectors);
            $joinDirectors = '';
            $valuesDirectors = '';
          }
        }
      }
      if(!empty($valuesMovies))
      {
        $this->db->query($insertMoviesStart.$valuesMovies.$insertMoviesEnd);
        $this->db->query('UPDATE movie_directors md INNER JOIN directors d ON md.id_director=d.id_director
                          SET d.need_details=1
                          WHERE md.id_tmdb in ('.implode(',', array_map(array($this->db,'escape'), $keyList)).')');
        $this->db->query('DELETE FROM movie_directors WHERE id_tmdb in ('.implode(',', array_map(array($this->db,'escape'), $keyList)).')');
        if(!empty($valuesDirectors))
        {
          $this->db->query($insertDirectorsStart.$valuesDirectors);
        }
      }
      $this->db->query("COMMIT");
    }
  }

  // rendezoi adatlapok kitoltese, frissitese
  public function updateDirectorsDetail($details = array())
  {
    if(!empty($details) AND is_array($details))
    {
      $this->db->query('BEGIN');
      $insertDirectorsStart = 'INSERT INTO directors (id_director,name,biography,date_of_birth,need_details) VALUES ';
      $insertDirectorsEnd = ' ON DUPLICATE KEY UPDATE name=VALUES(name),biography=VALUES(biography),
                              date_of_birth=VALUES(date_of_birth),need_details=VALUES(need_details)';
      $valuesDirectors = '';
      $joinDirectors = '';
      foreach($details as $id_director => $detail)
      {
        $valuesDirectors.=$joinDirectors.'('.$this->db->escape($id_director).','
                            .$this->db->escape($detail['name']).','
                            .$this->db->escape($detail['biography']).','
                            .$this->db->escape($detail['birthday'])
                            .',0)';
        $joinDirectors = ', ';
        if(strlen($valuesDirectors) > $this->batchSize)
        {
          $this->db->query($insertDirectorsStart.$valuesDirectors.$insertDirectorsEnd);
          $joinDirectors = '';
          $valuesDirectors = '';
        }
      }
      if(!empty($valuesDirectors))
      {
        $this->db->query($insertDirectorsStart.$valuesDirectors.$insertDirectorsEnd);
      }
      $this->db->query("COMMIT");
    }
  }

  // frissitendo film adatlapok listajabol visszaadunk 50 db-t
  public function getMoviesForUpdate($limit = 50)
  {
    $movies = array();
    $query = $this->db->query('SELECT id_tmdb FROM movies WHERE need_details=1 LIMIT '.(int)$limit);
    if($query->num_rows() > 0)
    {
      $result = $query->result_array();
      foreach($result as $row)
      {
        $movies []= $row['id_tmdb'];
      }
    }
    return $movies;
  }

  // frissitendo rendezoi adatlapok listajabol visszaadunk 50 db-t
  public function getDirectorsForUpdate($limit = 50)
  {
    $directors = array();
    $query = $this->db->query('SELECT distinct m.id_director FROM movie_directors m
                               LEFT JOIN directors d ON m.id_director=d.id_director WHERE d.need_details=1 or d.need_details is null LIMIT '.(int)$limit);
    if($query->num_rows() > 0)
    {
      $result = $query->result_array();
      foreach($result as $row)
      {
        $directors []= $row['id_director'];
      }
    }
    return $directors;
  }

  // ma volt-e mar toplista lekerdezes?
  public function requestTopRatedMovies()
  {
    $result = false;
    $query = $this->db->query('SELECT last_run,run_at FROM cron WHERE code="getTopRatedMovies"');
    $cron = $query->row_array();
    if(!empty($cron['run_at']) AND (empty($cron['last_run']) OR ($cron['last_run'] < date('Y-m-d ').$cron['run_at'])))
    {
      $result = true;
    }
    return $result;
  }

  // lefutott a toplista frissites, ma mar ne kerdezze le tobbszor
  public function requestTopRatedMoviesDone()
  {
    $this->db->query('UPDATE cron SET last_run=NOW() WHERE code="getTopRatedMovies"');
  }

  // mikor volt az utolso toplista frissites?
  public function getLastUpdate()
  {
    $result = '';
    $query = $this->db->query('SELECT last_run FROM cron WHERE code="getTopRatedMovies"');
    $cron = $query->row_array();
    if(!empty($cron['last_run']))
    {
      $result = $cron['last_run'];
    }
    return $result;
  }

  // elso 10 film a teszthez
  public function getTop10()
  {
    $result = array();
    $query = $this->db->query('SELECT m.position,m.id_tmdb,m.title,m.release_date,m.overview,m.tmdb_url,m.poster_url,
                m.vote_average,m.vote_count,GROUP_CONCAT(DISTINCT g.genre  ORDER BY g.genre SEPARATOR ", ") as genres,
                GROUP_CONCAT(DISTINCT d.name  ORDER BY d.name SEPARATOR ", ") as directors,m.backdrop_url
                FROM movies m
                LEFT JOIN movie_genres mg ON m.id_tmdb=mg.id_tmdb
                LEFT JOIN genres g ON mg.id_genre=g.id_genre
                LEFT JOIN movie_directors md ON md.id_tmdb=m.id_tmdb
                LEFT JOIN directors d ON md.id_director=d.id_director
                GROUP by m.id_tmdb
                ORDER BY m.position LIMIT 10');
    return $query->result_array();
  }

  // adott film adatlapjanak a lekerdeze
  public function getMovie($id)
  {
    $result = array();
    $query = $this->db->query('SELECT m.position,m.id_tmdb,m.title,m.release_date,m.overview,m.tmdb_url,m.poster_url,
                m.vote_average,m.vote_count,m.runtime,m.backdrop_url
                FROM movies m
                WHERE id_tmdb='.$this->db->escape($id));
    $result = $query->row_array();
    // valaszba becsatoljuk a tipusokat
    $query = $this->db->query('SELECT g.genre FROM movie_genres mg
                               INNER JOIN genres g ON mg.id_genre=g.id_genre
                               WHERE mg.id_tmdb='.$this->db->escape($id).'
                               ORDER by g.genre');
    $genres = $query->result_array();
    $result['genres'] = array();
    foreach($genres as $genre)
    {
      $result['genres'] []= $genre['genre'];
    }
    // valaszhoz adjuk a rendezoket is
    $query = $this->db->query('SELECT d.name,d.date_of_birth,d.biography FROM movie_directors md
                               INNER JOIN directors d ON md.id_director=d.id_director
                               WHERE md.id_tmdb='.$this->db->escape($id).'
                               ORDER by d.name');
    $result['directors'] = $query->result_array();
    return $result;
  }

  // a toplistabol lekerdezunk egy oldalt oldalmeret szerint
  public function getMovies($page = 1 , $pageSize = 30)
  {
    $result = array();
    $query = $this->db->query('SELECT m.position,m.id_tmdb,m.title,m.release_date,m.overview,m.tmdb_url,m.poster_url,
                m.vote_average,m.vote_count,GROUP_CONCAT(DISTINCT g.genre  ORDER BY g.genre SEPARATOR ", ") as genres,
                m.backdrop_url,m.runtime
                FROM movies m
                LEFT JOIN movie_genres mg ON m.id_tmdb=mg.id_tmdb
                LEFT JOIN genres g ON mg.id_genre=g.id_genre
                GROUP by m.id_tmdb
                ORDER BY m.position LIMIT '.(((int)$page-1) * $pageSize).','.(int)$pageSize);
    $result = $query->result_array();
    return $result;
  }

  // filmek szama az oldalak szamitasahoz
  public function getMovieCount()
  {
    $query = $this->db->query('SELECT count(*) as db FROM movies');
    $row = $query->row_array();
    $result = empty($row['db']) ? 0 : $row['db'];
    return $result;
  }

}
